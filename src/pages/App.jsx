import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import 'bulma/css/bulma.css'

import './App.css'
import Index from './index'
import Meeting from './meeting'
import Redirect from '../components/Redirect/Redirect'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="full">
          <Route path="/meeting/:meetingId" component={Meeting} />
          <Route path='/' component={Index}/>
        </div>
      </Router>
    )
  }
}

export default App
