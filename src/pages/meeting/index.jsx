import React from "react";
import * as Cookies from "js-cookie";

import "./meeting.css";
import AgoraVideoCall from "../../components/AgoraVideoCall";
import { AGORA_APP_ID } from "../../agora.config";

class Meeting extends React.Component {
  constructor(props) {
    super(props);
    this.videoProfile = "480p_4" //Cookies.get("videoProfile").split(",")[0] || "480p_4";
    this.channel = this.props.match.params.meetingId
    this.transcode = "interop" //Cookies.get("transcode") || "interop";
    this.attendeeMode = "video" //Cookies.get("attendeeMode") || "video";
    this.baseMode = "avc" //Cookies.get("baseMode") || "avc";
    
    console.log(this.videoProfile)
    console.log(this.transcode)
    console.log(this.attendeeMode)
    console.log(this.baseMode)
    //this.displayMessage = "Please wait while we are connecting the call..."
    this.appId = AGORA_APP_ID;
    if (!this.appId) {
      return alert("Get App ID first!");
    }
    this.state = {
      displayMessage : "Please wait while we are connecting to your call..."
    }
    this.OnUserJoin = this.OnUserJoin.bind(this)
    this.uid = undefined;
  }
  OnUserJoin() {
    console.log("user joined")
    this.setState({
      displayMessage: "Connected"
    })
  }
  render() {
    return (
      <div className="wrapper meeting">
        <div className="ag-header">
          <div className="ag-header-lead">
            <img
              className="header-logo"
              src={require("../../assets/images/ag-logo.png")}
              alt=""
            />
          <span>{this.state.displayMessage}</span>
          </div>
{/*           <div className="ag-header-msg">
            Room:&nbsp;<span id="room-name">{this.channel}</span>
          </div> */}
        </div>
        <div className="ag-main">
          <div className="ag-container">
            <AgoraVideoCall
              videoProfile={this.videoProfile}
              channel={this.channel}
              transcode={this.transcode}
              attendeeMode={this.attendeeMode}
              baseMode={this.baseMode}
              appId={this.appId}
              uid={this.uid}
              bothUsersJoined={this.OnUserJoin}
            />
          </div>
        </div>
        <div className="ag-footer">
          {/* <a className="ag-href" href="https://www.agora.io">
            <span>Powered By Agora</span>
          </a>
          <span>Talk to Support: 400 632 6626</span> */}
        </div>
      </div>
    );
  }
}

export default Meeting;
